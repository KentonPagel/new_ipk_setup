#!/usr/bin/python
"""
Sets up things on the LMU.
"""

import os
import sys
from datetime import datetime
import logging
import argparse
import ConfigParser

ORIGINAL_WD = None

# ee_logging_ini
# LIST_OF_EE_LOGS_TO_SET_TO_DEBUG =
#     ['ErrorReporter', 'CloudProvisioning', 'EngineData', 'OBD2',
#      'PowerUnitWifiConfig', 'TelematicsUpdate', 'Location', 'ApiRequests',
#      'TabletSocket', 'EngineEventsService', 'SensorFailureMonitor',
#      'SelfMonitor', 'TelematicsStoreAndForward', 'OvernightDataRecorder',
#      'JPod2', 'CritEvtRecorder', 'CsvLogger', 'DriverPerformanceMonitor',
#      'EngineFaultReporter', 'MotionData', 'Actions', 'GearInfoCSV',
#      'AutoVue', 'Bendix', 'HandlerLogsEE', 'SocketLogsEE', 'HSLClient',
#      'ScanbusReporter', 'MqttClient', 'TireMonitoring', 'EngineDataValidator',
#      'VT100', 'TestMonitor']
LIST_OF_EE_LOGS_TO_SET_TO_DEBUG = ['CritEvtRecorder', # EngineData
                                   'ApiRequests', # CritEvtRecorder
                                   'SensorFailureMonitor',
                                   'EngineData',
                                   'SelfMonitor',
                                   'TabletSocket']  # TabletSocket, etc 
LIST_OF_EE_LOGS_TO_SET_TO_INFO = ['']   # EngineData
LIST_OF_EE_LOGS_TO_SET_TO_WARNING = ['EngineData', '']
LIST_OF_EE_LOGS_TO_SET_TO_ERROR = ['']

# hsl_logging_ini loggers:
# LIST_OF_HSL_LOGS_TO_SET_TO_DEBUG =
#     ['AccessorLogs', 'HandlerLogsHSL', 'SocketLogsHSL',
#      'HardwareSupportLayer', 'PerfLogsHSL']
LIST_OF_HSL_LOGS_TO_SET_TO_DEBUG = ['AccessorLogs']
LIST_OF_HSL_LOGS_TO_SET_TO_INFO = ['']  # AccessorLogs
LIST_OF_HSL_LOGS_TO_SET_TO_WARNING = ['']
LIST_OF_HSL_LOGS_TO_SET_TO_ERROR = ['']

# LIST_OF_SIMULATE_EE_LOGS_TO_SET_TO_DEBUG = ['SimulateEE', 'JPod2']
LIST_OF_SIMULATE_EE_LOGS_TO_SET_TO_DEBUG = ['']
LIST_OF_SIMULATE_EE_LOGS_TO_SET_TO_INFO = ['SimulateEE']
LIST_OF_SIMULATE_EE_LOGS_TO_SET_TO_WARNING = ['']
LIST_OF_SIMULATE_EE_LOGS_TO_SET_TO_ERROR = ['']


def execute_command(os_cmd=""):
    """
    Runs system commands on the OS.
    """
    print("-------------------------------Calling '{}'...".format(os_cmd))
    os.system(os_cmd)
    print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^Completed '{}'!!".format(os_cmd))

def provision_device_master():
    """
    set url
    """
    found_device_master_section = False
    new_data = []
    with open("/user1/pltsci/provisioning.json") as f:
        for line in f.readlines():
            if 'device_master' in line:
                found_device_master_section = True
            if found_device_master_section and 'url' in line:
                new_url_line = \
                        '    "url": "https://devices-dev.pltsci.com:443",\n'
                new_data += new_url_line
                print("\nModifying device_master url to: {}".format(
                    new_url_line))
            else:
                new_data += line

    with open("/user1/pltsci/provisioning.json", 'w') as file:
        file.writelines(new_data) 

def set_log_levels():
    """
    Set log levels to DEBUG, INFO, or leave alone based on DEFINES above.
    """
    print("------------------------------- Setting log levels...")
    list_of_logger_config_files = list()
    list_of_logger_config_files.append('/user1/pltsci/ee_logging.ini')
    list_of_logger_config_files.append('/user1/pltsci/hsl_logging.ini')
    list_of_logger_config_files.append('/user1/pltsci/simulate/logging.ini')

    for config_file in list_of_logger_config_files:
        config = ConfigParser.ConfigParser()
        try:
            config.read(config_file)
            loggers = config.get("loggers", "keys").split(',')
        except Exception as error:
            print("\tWARNING!{} not found: {}".format(config_file, error))
            continue

        # Set log level for each logger defined.
        for log_name in loggers:
            logger = logging.getLogger(log_name)
            log_item = "logger_" + log_name
            try:
                if (log_name in LIST_OF_EE_LOGS_TO_SET_TO_DEBUG
                        or log_name in LIST_OF_HSL_LOGS_TO_SET_TO_DEBUG
                        or log_name in LIST_OF_SIMULATE_EE_LOGS_TO_SET_TO_DEBUG):
                    logger.setLevel(getattr(logging, 'DEBUG'))
                    config.set(log_item, "level", 'DEBUG')
                    print("In {}, setting {} to {}!".format(config_file, log_name, 'DEBUG'))
                elif (log_name in LIST_OF_EE_LOGS_TO_SET_TO_INFO
                        or log_name in LIST_OF_HSL_LOGS_TO_SET_TO_INFO
                        or log_name in LIST_OF_SIMULATE_EE_LOGS_TO_SET_TO_INFO):
                    logger.setLevel(getattr(logging, 'INFO'))
                    config.set(log_item, "level", 'INFO')
                    print("In {}, setting {} to {}!".format(config_file, log_name, 'INFO'))
                elif (log_name in LIST_OF_EE_LOGS_TO_SET_TO_WARNING
                        or log_name in LIST_OF_HSL_LOGS_TO_SET_TO_WARNING
                        or log_name in LIST_OF_SIMULATE_EE_LOGS_TO_SET_TO_WARNING):
                    logger.setLevel(getattr(logging, 'WARNING'))
                    config.set(log_item, "level", 'WARNING')
                    print("In {}, setting {} to {}!".format(config_file, log_name, 'WARNING'))
                elif (log_name in LIST_OF_EE_LOGS_TO_SET_TO_ERROR
                        or log_name in LIST_OF_HSL_LOGS_TO_SET_TO_ERROR
                        or log_name in LIST_OF_SIMULATE_EE_LOGS_TO_SET_TO_ERROR):
                    logger.setLevel(getattr(logging, 'ERROR'))
                    config.set(log_item, "level", 'ERROR')
                    print("In {}, setting {} to {}!".format(config_file, log_name, 'ERROR'))
                else:
                    try:
                        current_level = config.get(log_item, "level")
                        if (current_level == "ERROR" or current_level == "WARNING"):
                            # Do not clear if it's ERROR or WARNING
                            print("In {}, keeping logging for {} at {}!".format(
                                config_file, log_name, current_level))
                            continue
                        config.remove_option(log_item, "level")
                        print("In {}, disabling logging for {}!".format(config_file, log_name))
                    except ConfigParser.NoOptionError:
                        print("In {}, no logging enabled for {}!".format(config_file, log_name))
            except AttributeError:
                print("Could not determine correct level from {}".format('DEBUG'))

        with open(config_file, 'w') as current_config_file:
            config.write(current_config_file)
    print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Done setting log levels!")


def validate_arguments(args_passed_in):
    """
    Validate combo of arguments
    """
    global ORIGINAL_WD
    if os.getcwd() != '/user1/pltsci':
        os.chdir("/user1/pltsci")
        ORIGINAL_WD = '/user1/pltsci'
    print("\r\nStatus of the following arguments are:")
    for argument in vars(args_passed_in).items():
        print("\t{:<25} - {}".format(argument[0], argument[1]))
    if (args_passed_in.install_41d is not None
            and args_passed_in.install_41f is not None
            and args_passed_in.install_41g is not None):
        print("ERROR! Selected more than one software version, select one...")
        sys.exit(-1)


def handle_toggle_metrics(toggle):
    """
    Toggle the metrics.
    """
    if toggle is not None:
        execute_command("./toggle_metrics.sh {}".format(toggle))
    else:
        print("\r\n\tDid not pass in --toggle=xxx, NOT running ./toggle_metrics.sh\r\n")


def potentially_install_41d(install_41d):
    """
    Install stuff for 4.1D
    """
    try:
        os.chdir(
            "/user1/5541-41d-install/LMU5541_Release_AppID_166_v41d_Aug_19_2020/")
    except OSError:
        print("\r\nERROR: Directory /5541-41d-install/ does not exist!!")

    if install_41d == "jpod-fw":
        print("WARNING: Disable CAN simulator!")
        execute_command("cp JPOD2-008-12q.bin /user1/ota-dnld-file")
        execute_command("calamp_cli -o 'at$app fprog 8 0'")
    elif install_41d == "jpod-config":
        print("WARNING: Disable CAN simulator!")
        execute_command("cp JPod2_Config_054-Cummins-Faults-CRC.csv /user1/ota-dnld-file")
        execute_command("calamp_cli -o 'at$app fprog 8 1'")
    elif install_41d == "lmu-fw":
        execute_command("cp LMU-LTE-166-41d.bin /user1/ota-dnld-file")
        execute_command("/etc/init.d/fw_upgrade start")
    else:
        print("\r\nnot running install-41d.py, invalid option: {}\r\n".format(install_41d))
        return
    print("\r\n\tatcmd not available yet, installing lmu or jpod fw...")
    print("\r\n..poll [ tail -F ~/../var/log/messages ] for completion of updates!!\r\n")


def potentially_install_41f(install_41f):
    """
    Install stuff for 4.1F
    """
    try:
        os.chdir("/user1/41f-install-lmu55xx/")
    except OSError:
        print("\r\nERROR: Directory /41f-install-lmu55xx/ does not exist!"
              " Cannot run ./install-41f.sh\r\n")

    if install_41f == "yes":
        execute_command("sh install-41f.sh")
        print("\n\tNeed to reset to apply changes!")
        return
    elif install_41f == "jpod-config":
        print("WARNING: Disable CAN simulator!")
        execute_command(
            "cp JPod2_Config_180-from-054-Cummins-Faults-VHCL-711-CRC.csv"
            " /user1/ota-dnld-file")
        execute_command("calamp_cli -o 'at$app fprog 8 1'")
    elif install_41f == "jpod-fw":
        print("WARNING: Disable CAN simulator!")
        execute_command("cp JPODII-13f.bin /user1/ota-dnld-file")
        execute_command("calamp_cli -o 'at$app fprog 8 0'")
    elif install_41f == "lmu-fw-5541":
        execute_command("cp LMU-LTE-166-41f.bin /user1/ota-dnld-file")
        execute_command("/etc/init.d/fw_upgrade start")
    elif install_41f == "lmu-fw-5530":
        execute_command("cp LMU-LTE-164-41f.bin /user1/ota-dnld-file")
        execute_command("/etc/init.d/fw_upgrade start")
    else:
        print("\r\nnot running install-41f.sh, invalid option: {}\r\n".format(install_41f))
        return
    print("\r\n\tatcmd not available yet, installing lmu or jpod fw...")
    print("\r\n..poll [ tail -F /var/log/messages ] for completion of updates!!\r\n")


def potentially_install_41g(install_41g):
    """
    Install stuff for 4.1G
    """
    try:
        os.chdir("/user1/41g-install-lmu55xx/")
    except OSError:
        print("\r\nERROR: Directory /41g-install-lmu55xx/ does not exist!"
              " Cannot run ./install-41g.sh\r\n")

    if install_41g == "yes":
        execute_command("sh install-41g.sh")
        print("\n\tNeed to reset to apply changes!")
        return
    elif install_41g == "jpod-config":
        print("WARNING: Disable CAN simulator!")
        execute_command(
            "cp JPod2_Config_180-from-054-Cummins-Faults-VHCL-711-CRC.csv"
            " /user1/ota-dnld-file")
        execute_command("calamp_cli -o 'at$app fprog 8 1'")
    elif install_41g == "jpod-fw":
        print("WARNING: Disable CAN simulator!")
        execute_command("cp JPODII-13f.bin /user1/ota-dnld-file")
        execute_command("calamp_cli -o 'at$app fprog 8 0'")
    elif install_41g == "lmu-fw-5541":
        execute_command("cp LMU-LTE-166-41g.bin /user1/ota-dnld-file")
        execute_command("/etc/init.d/fw_upgrade start")
    elif install_41g == "lmu-fw-5530":
        execute_command("cp LMU-LTE-164-41g.bin /user1/ota-dnld-file")
        execute_command("/etc/init.d/fw_upgrade start")
    else:
        print("\r\nnot running install-41g.sh, invalid option: {}\r\n".format(install_41g))
        return
    print("\r\n\tatcmd not available yet, installing lmu or jpod fw...")
    print("\r\n..poll [ tail -F /var/log/messages ] for completion of updates!!\r\n")


def potentially_install_41i(install_41i):
    """
    Install stuff for 4.1I
    """
    try:
        os.chdir("/user1/41i-install-lmu55xx/")
    except OSError:
        print("\r\nERROR: Directory /41i-install-lmu55xx/ does not exist!"
              " Cannot run ./install-41i.sh\r\n")

    if install_41i == "yes":
        execute_command("sh install-41i.sh")
        print("\n\tNeed to reset to apply changes!")
        return
    elif install_41i == "lmu-fw-5541":
        execute_command("cp LMU-LTE-166-41i.bin /user1/ota-dnld-file")
        execute_command("/etc/init.d/fw_upgrade start")
    elif install_41i == "lmu-fw-5530":
        execute_command("cp LMU-LTE-164-41i.bin /user1/ota-dnld-file")
        execute_command("/etc/init.d/fw_upgrade start")
    else:
        print("\r\nnot running install-41i.sh, invalid option: {}\r\n".format(install_41i))
        return
    print("\r\n\tatcmd not available yet, installing lmu or jpod fw...")
    print("\r\n..poll [ tail -F /var/log/messages ] for completion of updates!!\r\n")


def install_ee_sim():
    """
    Install stuff for EE sim.
    """
    global ORIGINAL_WD
    os.chdir("/user1/")
    list_of_files_in_directory = os.listdir(os.getcwd())
    ipk_file = None
    for file_name in list_of_files_in_directory:
        if 'lmu5541.ipk' in file_name:
            ipk_file = file_name
    print("Using ee sim ipk file - {}".format(ipk_file))
    execute_command("opkg install {}".format(ipk_file))
    print("To run ee simulator, [ cd /user1/pltsci/simulate ] and [ ./simulate-ee.py ]")
    os.chdir(ORIGINAL_WD)


def enable_ssh_login():
    """
    Disable password requirement..
    """
    execute_command("cat calamp_id_rsa_v1.pub  >> /etc/dropbear/authorized_keys")
    execute_command("chmod 0600 /etc/dropbear/authorized_keys")
    print("Password requirement should be disabled!")


def configure_apn():
    """
    Configure APN
    """
    execute_command("calamp_cli -o 'at$app param 2306,0,10569.MCS'")
    execute_command("calamp_cli -o 'at$app peg action 55 0'")
    print("Configured the APN, now wait...")


if __name__ == '__main__':
    ORIGINAL_WD = os.getcwd()
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--lmu_login', help="'ssh'")
    arg_parser.add_argument('--toggle', help="Execute ./toggle_metrics.sh = 'enable' or 'disable'")
    # TODO add lmu-cfg option, 55.51?
    arg_parser.add_argument('--install_41i', help="'yes', 'lmu-fw-5541', 'lmu-fw-5530'")
    arg_parser.add_argument('--install_41g', help="'yes', 'lmu-fw-5541', 'lmu-fw-5530', 'jpod-config', 'jpod-fw'")
    arg_parser.add_argument('--install_41f', help="'yes', 'lmu-fw-5541', 'lmu-fw-5530', 'jpod-config', 'jpod-fw'")
    arg_parser.add_argument('--install_41d', help="'lmu-fw', 'jpod-config', 'jpod-fw'")
    arg_parser.add_argument('--install_ee_sim', help="'yes'")
    arg_parser.add_argument('--configure_apn', help="'yes'")
    args = arg_parser.parse_args()

    validate_arguments(args)
    handle_toggle_metrics(args.toggle)
    execute_command("/etc/init.d/ps_monitor stop")
    set_log_levels()  # Just set the levels regardless of what happens next
    provision_device_master()

    if (args.install_41d is None
            and args.install_41f is None
            and args.install_41g is None
            and args.install_41i is None
            and args.install_ee_sim is None
            and args.lmu_login is None
            and args.configure_apn is None):
        execute_command("/etc/init.d/ps_monitor restart")
        print("\r\n-----Using LMU configuration:")
        os.system("calamp_cli -o 'ats121?'")
        os.system("calamp_cli -o 'ats143?'")

        print("\r\n-----J1939/OBD2 config (13 vs 9)")
        os.system("calamp_cli -o 'ats178=13'")  # 13 = J1939, 9 = OBD2
        os.system("calamp_cli -o 'ats178?'")

        print("\r\n-----Using software versions:")
        os.system("atcmd ati0")
        os.system("atcmd ativ")
        os.system("opkg list sandman")
        os.system("opkg list SimulateEE")
        os.system("opkg list UPM")
        os.system("opkg list HaPythonAPI")
        os.system("opkg list OBD2PackageInstaller")
        os.system("opkg list EngineEvents")
        print("\r\nTo see logs: [ cd /var/log/pltsci ]\r\n")
    elif args.install_ee_sim is not None:
        install_ee_sim()
    elif args.lmu_login is not None:
        enable_ssh_login()
    elif args.configure_apn is not None:
        configure_apn()
    elif args.install_41d is not None:
        potentially_install_41d(args.install_41d)
    elif args.install_41f is not None:
        potentially_install_41f(args.install_41f)
    elif args.install_41g is not None:
        potentially_install_41g(args.install_41g)
    elif args.install_41i is not None:
        potentially_install_41i(args.install_41i)
    print("{}\r\n".format(datetime.now()))
    print("^^^^^^^^^^^^^^^^^^^^^^^^^^\r\n")
