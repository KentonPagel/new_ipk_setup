#!/bin/bash

if [ "$#" -eq 1 ]; then

	if [[ $1 == *.ipk ]]; then

		filename=`basename $1 ipk`
		tar czf $filename.tar.gz $1
		mv $filename.tar.gz ${filename}bin
		exit 0
	fi
fi

echo "Usage:"
echo ""
echo "./makebin <lmu_ipk_file>"
echo ""
echo "example:"
echo "./makebin.sh SSHReverseTunnel_lmu5530-1.0.1-4.ipk"
