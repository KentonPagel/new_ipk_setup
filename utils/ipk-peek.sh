#!/bin/sh

# View contents of .ipk files
# Usage: run this shell script in a directory with the .ipk file.

mkdir -p extract
cd extract 
tar xfz ../*.ipk
tar tvfz data.tar.gz | awk '{print $1,$2,$3,$4,$5,$6,$7,$8,$9}'
rm -rf *
