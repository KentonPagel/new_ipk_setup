#!/bin/sh

echo "Installing packages for a freshly flashed LMU55xx with 41x"

echo "---------------------------------------"
echo "Install atcmd, postinst relies on this..."
echo "---------------------------------------"
cp atcmd /usr/bin/atcmd
chmod +x /usr/bin/atcmd

echo "removing Python3"
UNST_PKGS="python3-pkg-resources \
python3 \
python3-xml \
python3-urllib \
python3-unittest \
python3-sqlite3 \
python3-cgitb \
python3-pydoc \
python3-openssl \
python3-ncurses \
python3-multiprocessing \
python3-logging \
python3-gdbm \
python3-cgi \
python3-email \
python3-lzma \
python3-decimal \
python3-ctypes \
python3-codecs \
python3-asyncio \
python3-dbm \
python3-distutils \
python3-light \
python3-base \
libsqlite3 \
liblzma \
libgdbm \
libdb47 \
libxml2 \
libbz2"

# libncurses \

for pkg in $UNST_PKGS
do
	opt=""
#	if [ $pkg == "libncurses" ]; then
#		opt="--force-depends"
#	fi
	opkg remove $opt $pkg
done


echo "---------------------------------------"
echo "Installing Python, HAPY, and sqlite"
echo "---------------------------------------"
opkg install --force-downgrade *.ipk

echo "---------------------------------------"
echo "install LMU config 55.51"
echo "---------------------------------------"
cp 55-51-PlatformScience-Sandman-CRC.csv /user1/ota-dnld-file
atcmd 'atdscfg'

echo "---------------------------------------"
echo "configure stream settings"
echo "---------------------------------------"
atcmd 'at$app param 3072,3,1'
atcmd 'at$app param 3072,7,0'

echo "---------------------------------------"
echo "turn off comm and VBus debug"
echo "---------------------------------------"
atcmd at app param 3722,2,0
atcmd at app param 3722,1,0

echo " "
echo "---------------------------------------"
echo "Done installing!"
echo ""
echo "*** reboot LMU for all settings to  be applied! ***"
echo ""
echo "Please install latest JPod2 config found in 41f-install/JPOD2-008z.bin if you have a JPod2 that needs it"
echo "this is the latest JPod2 13f beta (1.3f) firmare"
echo ""
echo "Update JPod2 with these commands:"
echo "cp JPODII-13f.bin /user1/ota-dnld-file"
echo "atcmd at app fprog 8 0"
echo "---------------------------------------"
echo " "

exit 0
