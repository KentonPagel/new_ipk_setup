# README #

After installing a new IPK, some steps are necessary, such as:
    * configuring logger levels
    * enabling/disabling metrics
    * installing 41d/41f fw, scripts, configs
    * installing ee simulator
    * re-starting engine_events,
so this provides a script to automate this.

### How do I get set up? ###

This repo is dependent on the following repos:
    * 5530-basic-engine-events,
    * pltsci-ee-sim 
so this repo needs to live in the same location as them.

Usage:
python mac_setup.py --help   # From mac
python lmu_setup.py --help   # From lmu @ user1/pltsci

    # Upgrade JPOD FW:
scp -O -P 2001 41f-install-lmu55xx/JPODII-13f.bin root@192.168.1.55:/user1/ota-dnld-file
calamp_cli -o 'at$app fprog 8 0'

    # - Copy JPOD configs:
scp -O -P 2001 <filename> root@192.168.1.55:/user1/ota-dnld-file
calamp_cli -o 'at$app fprog 8 1' 

    # - Read performance metrics:
/etc/init.d/ps_monitor stop
cat /var/log/pltsci/hsl/hardware_support_layer.log
cat /var/log/pltsci/engine_events_service.log

    # Run EE by hand from /user1/pltsci
python /user1/pltsci/engine_events_app.py
python /user1/pltsci/hardware_support_layer.py

    # Upgrade LMU Config
scp -O -P 2001 41g-install-lmu55xx/55-51-PlatformScience-Sandman-CRC.csv root@192.168.1.55:/user1/ota-dnld-file
atcmd 'atdscfg'

    # Upgrade atcmd
cp atcmd /usr/bin/atcmd
chmod +x /usr/bin/atcmd


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

