#!/usr/bin/python

import os
import sys
import argparse
import pexpect
import subprocess
from datetime import datetime

ORIGINAL_WD = None
NEED_TO_USE_PASSWORD = True


def execute_command(os_cmd="", capture_cmd=False):
    """
    Execute command to interface with LMU, sometimes a default password is required.
    :param os_cmd: command to execute
    :param capture_cmd: capture standard outptut response on LMU
    :return: any output for the command.
    """
    global NEED_TO_USE_PASSWORD
    return_value = ""

    print("-------------------------------Calling '{}'...".format(os_cmd))
    if NEED_TO_USE_PASSWORD:
        lmu_default_password = "wireless3g"
        print("WARNING! Need to use default password: {}".format(
            lmu_default_password))
        try:
            child = pexpect.spawn("{}".format(os_cmd), timeout=10)
            child.expect([pexpect.TIMEOUT, "root@192.168.1.55's password:",
                         pexpect.EOF],
                         timeout=10)
            child.sendline(lmu_default_password)
            child.expect([pexpect.TIMEOUT, "\r\n", pexpect.EOF], timeout=10)
            child.interact()
            child.close()
            return_value = child.before
        except OSError as error:
            print("Error: {}".format(type(error)))
            if error[1] == 'Input/output error':
                NEED_TO_USE_PASSWORD = False
                print("Password is using ssh key, disabling default password.")
            else:
                print("EXCEPTION! - {}".format(error))
    elif capture_cmd:
        return_value = subprocess.check_output("{}".format(os_cmd), shell=True)
    else:
        return_value = os.system(os_cmd)
    print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^Completed '{}'!!".format(os_cmd))
    return return_value


def remove_host_key_for_old_lmu():
    """
    Delete host key for old LMU
    """
    KNOWN_HOSTS_FILENAME = "~/.ssh/known_hosts"
    try:
        with open(os.path.expanduser(KNOWN_HOSTS_FILENAME), "r"):
            print("Found file {}!".format(KNOWN_HOSTS_FILENAME))
    except IOError:
        print("known_hosts file is not created...")
        sys.exit(-1)

    with open(os.path.expanduser(KNOWN_HOSTS_FILENAME), "r+") as file:
        lines = file.readlines()
        index_to_remove = -1
        for i, line_item in enumerate(lines):
            if "[192.168.1.55]" in line_item:
                index_to_remove = i
        if index_to_remove >= 0:
            lines.pop(index_to_remove)
    with open(os.path.expanduser(KNOWN_HOSTS_FILENAME), "w") as file:
        # Re-open the file as not to keep appending and doubling file size...
        for line in lines:
            file.write(line)


def add_host_key_for_new_lmu():
    """
    Add host key for calamp LMU.
    """
    calamp_password = "KkkuV2eVi3rqfdB"
    try:
        child = pexpect.spawn("ssh-add {}".format(
            os.path.expanduser("~/.ssh/calamp_id_rsa_v1")))
        child.expect([
            pexpect.TIMEOUT,
            "Enter passphrase for /Users/kentonpagel/.ssh/calamp_id_rsa_v1:",
            pexpect.EOF])
        print("{}".format(child.after))
        child.sendline(calamp_password)
        child.expect([
            pexpect.TIMEOUT,
            "Identity added: /Users/kentonpagel/.ssh/calamp_id_rsa_v1 "
            "(/Users/kentonpagel/.ssh/calamp_id_rsa_v1)",
            pexpect.EOF])
        print("{}".format(child.before))
    except OSError as error:
        print("EXCEPTION! - {}".format(error))


def add_key_for_pican():
    """
    Add key for pican.
    """
    pican_password = "surf&sn0w"
    pican_ip_address = "192.168.8.161"
    try:
        child = pexpect.spawn("ssh-copy-id {}".format(
            pican_ip_address))
        child.expect([
            pexpect.TIMEOUT,
            "pi@{}'s password:".format(pican_ip_address),
            pexpect.EOF])
        print("{}".format(child.after))
        child.sendline(pican_password)
        child.expect([
            pexpect.TIMEOUT,
            "Number of key(s) added:",
            pexpect.EOF])
        print("{}".format(child.before))
    except OSError as error:
        print("EXCEPTION! - {}".format(error))

def install_ee_sim():
    """
    Intall ee simulator
    """
    global ORIGINAL_WD
    # TODO: Replace relative directory with submodules?
    os.chdir("../pltsci-ee-sim/opkg/")
    execute_command("make clean")
    execute_command("make -f Makefile")
    list_of_files_in_directory = os.listdir(os.getcwd())
    ipk_file = None
    for file_name in list_of_files_in_directory:
        if 'lmu5541.ipk' in file_name:
            ipk_file = file_name
    print("Sending ee sim ipk file - {}".format(ipk_file))
    execute_command("scp -O -P 2001 {} root@192.168.1.55:/user1".format(ipk_file))
    os.chdir(ORIGINAL_WD)


def check_if_we_use_ssh_or_default_pwd():
    """
    First time communicating w/LMU, determine if we can use ssh password for following commands.
    """
    execute_command(os_cmd="ssh -p 2001 root@192.168.1.55 'ls ../user1 | xargs echo'",
                    capture_cmd=True)


def validate_pltsci_folder_existance():
    """
    Make sure folder is there!
    """
    response = execute_command(
                   os_cmd="ssh -p 2001 root@192.168.1.55 'ls ../user1 | xargs echo'",
                   capture_cmd=True)
    user1_folder = response.split(" ")
    for directory in user1_folder:
        if 'pltsci' in directory:
            break
    else:
        print("pltsci does not exist, creating...")
        execute_command("ssh -p 2001 root@192.168.1.55 'mkdir /user1/pltsci'")


def handle_swapping_lmu():
    """
    Setup password for new LMU, exit script.
    """
    remove_host_key_for_old_lmu()
    add_host_key_for_new_lmu()
    sys.exit(0)


def automate_pican_login():
    """
    Setup password for pican, exit script.
    """
    add_key_for_pican()
    sys.exit(0)


def initialize_for_session():
    """
    Set up for session,  check user1/pltsci folder, etc.
    """
    check_if_we_use_ssh_or_default_pwd()
    validate_pltsci_folder_existance()


def parse_arguments():
    """
    Parse arguments from command line, return them.
    """
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--install_41d', help="'yes'")
    arg_parser.add_argument('--install_41f', help="'yes")
    arg_parser.add_argument('--install_41g', help="'yes")
    arg_parser.add_argument('--install_upm', help="'yes")
    arg_parser.add_argument('--install_ee_sim', help="'yes")
    arg_parser.add_argument('--swap_lmu', help="'yes")
    arg_parser.add_argument('--pi_can_add_key', help="'yes")
    args = arg_parser.parse_args()

    print("\r\nStatus of the following arguments are:")
    for argument in vars(args).items():
        print("\t{:<25} - {}".format(argument[0], argument[1]))
    return args


if __name__ == '__main__':
    ORIGINAL_WD = os.getcwd()
    args = parse_arguments()
    if args.swap_lmu == "yes":
        handle_swapping_lmu()
    if args.pi_can_add_key == "yes":
        automate_pican_login()

    initialize_for_session()

    if args.install_41g == "yes":
        execute_command("scp -O -r -P 2001 41g-install-lmu55xx root@192.168.1.55:/user1/")
    elif args.install_41f == "yes":
        execute_command("scp -O -r -P 2001 41f-install-lmu55xx root@192.168.1.55:/user1/")
    elif args.install_41d == "yes":
        execute_command("scp -O -r -P 2001 5541-41d-install root@192.168.1.55:/user1/")
    elif args.install_ee_sim == "yes":
        install_ee_sim()
    elif args.install_upm == "yes":
        execute_command("scp -O -P 2001 upm_package/UPM_3.22.34-41_all_PULS_OTA.ipk "
                        "root@192.168.1.55:/user1/")
        print("Now on LMU...")
        print("[ cd /user1 ]")
        print("[ opkg install UPM_3.22.34-41_all_PULS_OTA.ipk ]")
        sys.exit(0)
    else:
        print("\r\n No options selected.")

    execute_command("scp -O -P 2001 lmu_setup.py "
                    "root@192.168.1.55:/user1/pltsci")
    execute_command("scp -O -P 2001 ../5530-basic-engine-events/toggle_metrics.sh "
                    "root@192.168.1.55:/user1/pltsci")
    execute_command("scp -O -P 2001 calamp_id_rsa_v1.pub "
                    "root@192.168.1.55:/user1/pltsci")

    print("\r\nNow on LMU, run these commands:")
    print("[ cd /user1/pltsci ]")
    print("[ python lmu_setup.py ]\r\n")
    print("{}\r\n".format(datetime.now()))

