#!/bin/sh

echo "Fix python3"

echo "removing Python3"
UNST_PKGS="python3-pkg-resources \
python3 \
python3-xml \
python3-urllib \
python3-unittest \
python3-sqlite3 \
python3-cgitb \
python3-pydoc \
python3-openssl \
python3-ncurses \
python3-multiprocessing \
python3-logging \
python3-gdbm \
python3-cgi \
python3-email \
python3-lzma \
python3-decimal \
python3-ctypes \
python3-codecs \
python3-asyncio \
python3-dbm \
python3-distutils \
python3-light \
python3-base \
libsqlite3 \
liblzma \
libgdbm \
libdb47 \
libxml2 \
libbz2"

# libncurses 
for pkg in $UNST_PKGS
do
	opt=""
#	if [ $pkg == "libncurses" ]; then
#		opt="--force-depends"
#	fi
	opkg remove $opt $pkg
done


echo "---------------------------------------"
echo "Installing Python 2"
echo "---------------------------------------"
opkg install --force-downgrade *.ipk

exit 0
